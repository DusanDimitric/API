'use strict'

const Boom = require('boom')

module.exports = error => {
  let err = new Error(error.name)
  return Boom.boomify(err, {
    statusCode: 422,
    decorate: {
      validationErrors: error.details.map(d => Object({
        "message" : d.message,
        "key"     : d.context.key,
      }))
    }
  })
}

