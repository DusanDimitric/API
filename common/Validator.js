'use strict'

const Joi = require('joi')

const ValidationError = require('^common/errors/ValidationError')

function validate(payload, schema) {
  const { error } = Joi.validate(payload, schema)
  if (error) {
    throw ValidationError(error)
  }
}

module.exports = {
  validate
}
