'use strict'

const R     = require('ramda')
const rooty = require('rooty')

rooty()

const { describe, test } = exports.lab = require('lab').script()
const { expect } = require('code')

const RPSGame       = require('^domain/RPS/Game')
const PlayerFactory = require('^domain/RPS/factories/PlayerFactory')

describe('Game.js', () => {

  describe('Match outcomes', () => {

    describe('Two player games', () => {

      const weaponPairs = [
        ['Rock',     'Scissors'],
        ['Scissors', 'Paper'   ],
        ['Paper',    'Rock'    ],
      ]
      weaponPairs.forEach(pair => {
        test(`${pair[0].padEnd(8)} beats ${pair[1].padEnd(8)}`, () => {
          const mockPlayers = [
            PlayerFactory.CreatePlayer({
              "name"   : "Player One",
              "weapon" : pair[0],
            }),
            PlayerFactory.CreatePlayer({
              "name"   : "Player Two",
              "weapon" : pair[1],
            }),
          ]
          const game = new RPSGame(mockPlayers)

          const { outcome, winner, survivors } = game.playMatch()

          expect(outcome).to.equal('Win')
          expect(R.equals(winner, mockPlayers[0])).to.be.true()
          expect(winner.points).to.equal(1)
          expect(survivors.length).to.equal(1)
          expect(R.equals(survivors[0], mockPlayers[0])).to.be.true()
        })
      })

      const weapons = ['Rock', 'Paper', 'Scissors']
      weapons.forEach(weapon => {
        test(`${weapon.padEnd(8)} against ${weapon.padEnd(8)} results in a "draw"`, () => {
          const mockPlayers = [
            PlayerFactory.CreatePlayer({
              "name"   : "Player One",
              "weapon" : weapon,
            }),
            PlayerFactory.CreatePlayer({
              "name"   : "Player Two",
              "weapon" : weapon,
            }),
          ]
          const game = new RPSGame(mockPlayers)

          const { outcome, winner, survivors } = game.playMatch()

          expect(outcome).to.equal('Draw')
          expect(winner).to.be.null()
          expect(survivors.length).to.equal(2)
          expect(R.equals(survivors, mockPlayers)).to.be.true()
        })
      })

    })

    describe('Multiplayer games', () => {

      test('Multiple survivors', () => {
        const mockPlayers = [
          PlayerFactory.CreatePlayer({
            "name"   : "Player One",
            "weapon" : "Rock",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Two",
            "weapon" : "Rock",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Five",
            "weapon" : "Paper",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Three",
            "weapon" : "Scissors",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Four",
            "weapon" : "Scissors",
          }),
        ]
        const game = new RPSGame(mockPlayers)

        const { outcome, winner, survivors } = game.playMatch()

        expect(outcome).to.equal('Draw')
        expect(winner).to.be.null()
        expect(survivors.length).to.equal(3)
        expect(R.equals(survivors, [ mockPlayers[0], mockPlayers[1], mockPlayers[2] ])).to.be.true()
        survivors.forEach(survivor => {
          expect(survivor.points).to.equal(2)
        })
      })

      test('Single survivor (the winner)', () => {
        const mockPlayers = [
          PlayerFactory.CreatePlayer({
            "name"   : "Player One",
            "weapon" : "Rock",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Two",
            "weapon" : "Scissors",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Five",
            "weapon" : "Paper",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Three",
            "weapon" : "Scissors",
          }),
          PlayerFactory.CreatePlayer({
            "name"   : "Player Four",
            "weapon" : "Scissors",
          }),
        ]
        const game = new RPSGame(mockPlayers)

        const { outcome, winner, survivors } = game.playMatch()

        expect(outcome).to.equal('Win')
        expect(R.equals(winner, mockPlayers[0])).to.be.true()
        expect(survivors.length).to.equal(1)
        expect(R.equals(survivors[0], mockPlayers[0])).to.be.true()
        expect(winner.points).to.equal(3)
      })

    })

  })

})

