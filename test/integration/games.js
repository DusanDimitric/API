'use strict'

const rooty = require('rooty')
rooty()

const { describe, it } = exports.lab = require('lab').script()
const { expect } = require('code')
const Joi = require('joi')

const server       = require('^server')
const PlayerSchema = require('^validation/schemas/Player')

describe('Games', () => {

  describe('CvC', () => {
    const oneToTen = Array.from(new Array(10), (val, index) => index + 1)

    oneToTen.forEach(i => {
      it(`Creates a new game #${i}`, async () => {

        const request = {
          method : 'POST',
          url    : '/games/cvc'
        }

        const response = await server.inject(request)
        const { result, statusCode } = response

        const { error } = Joi.validate(result, Joi.object({
          outcome   : Joi.string().required(),
          winner    : PlayerSchema.allow(null),
          survivors : Joi.array().items(PlayerSchema),
          players   : Joi.array().items(PlayerSchema)
        }))

        expect(statusCode).to.equal(201)
        expect(error).to.be.null()
      })
    })

  })

  describe('PvC', () => {

    const pvcResponseSchema = Joi.object({
      outcome   : Joi.string().required(),
      winner    : PlayerSchema.allow(null),
      survivors : Joi.array().items(PlayerSchema),
      players   : Joi.array().items(PlayerSchema)
    })

    it('Creates a new game given a minimum payload', async () => {

      const request = {
        method  : 'POST',
        url     : '/games/pvc',
        headers : {
          'Content-Type' : 'application/json'
        },
        payload : JSON.stringify({
          weapon : 'Rock'
        }),
      }

      const response = await server.inject(request)
      const { result, statusCode } = response

      const { error } = Joi.validate(result, pvcResponseSchema)
      expect(statusCode).to.equal(201)
      expect(error).to.be.null()
    })

    it('Creates a new game with a custom player name', async () => {

      const CUSTOM_PLAYER_NAME = 'RPSMaster'

      const request = {
        method  : 'POST',
        url     : '/games/pvc',
        headers : {
          'Content-Type' : 'application/json'
        },
        payload : JSON.stringify({
          weapon : 'Rock',
          name   : CUSTOM_PLAYER_NAME,
        }),
      }

      const response = await server.inject(request)
      const { result, statusCode } = response

      const { error } = Joi.validate(result, pvcResponseSchema)
      expect(statusCode).to.equal(201)
      expect(error).to.be.null()

      const playerName = JSON.parse(result).players
        .filter(p => p.name === CUSTOM_PLAYER_NAME)
        .map(p => p.name)[0]
      expect(playerName).to.equal(CUSTOM_PLAYER_NAME)
    })

    it('Creates a new game with 10 computer players', async () => {

      const NUM_COMPUTER_PLAYERS = 10
      const isComputerName = /^C\d+$/

      const request = {
        method  : 'POST',
        url     : '/games/pvc',
        headers : {
          'Content-Type' : 'application/json'
        },
        payload : JSON.stringify({
          weapon           : 'Rock',
          computer_players : NUM_COMPUTER_PLAYERS,
        }),
      }

      const response = await server.inject(request)
      const { result, statusCode } = response

      const { error } = Joi.validate(result, pvcResponseSchema)
      expect(statusCode).to.equal(201)
      expect(error).to.be.null()

      const resultObject = JSON.parse(result)
      expect(resultObject.players.length).to.equal(1 + NUM_COMPUTER_PLAYERS)

      const computerPlayers = resultObject.players.filter(player => isComputerName.test(player.name))
      expect(computerPlayers.length).to.equal(NUM_COMPUTER_PLAYERS)
    })

    const invalidPayloads = [
      { weXpon : 'Rock'  },
      { weapon : 'RockX' },
      { naem : 'My Name', weapon : 'Rock' },
      { name : 'My Name', weapon : 'Rock' }, // "name" contains a non-alphanum value
      { name : 33,        weapon : 'Rock' },
      { name : 'MyName',  weapon : 'Rock', other : 'extra property' },
      { name : 'MyName',  weapon : 'Rock', computer_players : '1a'  },
      { name : 'MyName',  weapon : 'Rock', computer_players : -3    },
      { name : 'MyName',  weapon : 'Rock', computer_players : 0     },
      { name : 'MyName',  weapon : 'Rock', computer_players : 101   },
    ]

    invalidPayloads
      .map(JSON.stringify)
      .forEach((payload, i) => {
        it(`Fails to create games with invalid payload #${i + 1}`, async () => {

          const request = {
            method  : 'POST',
            url     : '/games/pvc',
            headers : {
              'Content-Type' : 'application/json'
            },
            payload : payload,
          }

          const response = await server.inject(request)
          const { result, statusCode } = response

          expect(statusCode).to.equal(422)
          expect(result.message).to.equal('ValidationError')
        })
      })

  })

})

