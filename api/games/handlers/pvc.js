'use strict'

const RPSGame       = require('^domain/RPS/Game')
const PlayerFactory = require('^domain/RPS/factories/PlayerFactory')
const Validator     = require('^common/Validator')

const pvcPayloadSchema = require('../schemas/pvc-payload')

function create(request, h) {

  Validator.validate(request.payload, pvcPayloadSchema)

  const numComputerPlayers = request.payload.computer_players || 1

  const players = [
    PlayerFactory.CreatePlayer(request.payload),
    ...PlayerFactory.CreateComputerPlayers(numComputerPlayers),
  ]

  const game = new RPSGame(players)
  const { outcome, survivors, winner } = game.playMatch()

  return h
    .response(JSON.stringify({
      "outcome"   : outcome,
      "winner"    : winner,
      "survivors" : survivors,
      "players"   : players
    }))
    .code(201)
}

module.exports = {
  create
}

