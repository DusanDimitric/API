'use strict'

module.exports = [
  {
    path    : '/games/pvc',
    method  : 'POST',
    handler : require('../handlers/pvc').create,
  },
  {
    path    : '/games/cvc',
    method  : 'POST',
    handler : require('../handlers/cvc').create
  }
]

