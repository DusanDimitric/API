'use strict'

module.exports = (request, h) => {
  const response = request.response

  if (!response.isBoom) {
    return h.continue
  }

  if (response.validationErrors) {
    response.output.payload.validationErrors = response.validationErrors
  }

  return h.continue
}

