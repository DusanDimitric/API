'use strict'

module.exports = class RPSGame {

  constructor(players) {
    this.players = players
  }

  playMatch() {
    this.calculatePlayerPoints()
    const survivors = this.filterOutTheLosers()
    const { outcome, winner } = this.determineTheOutcome(survivors)
    return {
      outcome,
      survivors,
      winner,
    }
  }

  calculatePlayerPoints() {
    let player
    this.players.map(currentPlayer => {
      for (let i = 0; i < this.players.length; ++i) {
        player = this.players[i]
        if (player === currentPlayer) {
          continue
        }
        else if (currentPlayer.weapon.beats(player.weapon)) {
          ++currentPlayer.points
        }
      }
      return player
    })
  }

  filterOutTheLosers() {
    const maxPoints = this.players.reduce((max, player) => {
      if (player.points > max) {
        max = player.points
      }
      return max
    }, 0)
    return this.players.filter(player => {
      return player.points === maxPoints
    })
  }

  determineTheOutcome(survivors) {
    if (survivors.length === 1) {
      return {
        outcome : 'Win',
        winner  : survivors[0]
      }
    }
    else {
      return {
        outcome : 'Draw',
        winner  : null
      }
    }
  }

}

