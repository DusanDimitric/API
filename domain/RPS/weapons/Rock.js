'use strict'

const Weapon = require('../Weapon')

module.exports = class Rock extends Weapon {

  beats(otherWeapon) {
    return otherWeapon.constructor.name === 'Scissors'
  }

}
