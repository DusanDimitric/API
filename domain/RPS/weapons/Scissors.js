'use strict'

const Weapon = require('../Weapon')

module.exports = class Scissors extends Weapon {

  beats(otherWeapon) {
    return otherWeapon.constructor.name === 'Paper'
  }

}
