'use strict'

const NonExistentWeaponError = require('^./common/errors/NonExistentWeaponError')

const Rock     = require('../weapons/Rock')
const Paper    = require('../weapons/Paper')
const Scissors = require('../weapons/Scissors')

const weapons = [Rock, Paper, Scissors]

const WeaponFactory = {

  CreateWeapon(weaponName) {
    const Weapon = weapons.find(w => w.name.toLowerCase() === weaponName.toLowerCase())
    if (Weapon) {
      return new Weapon()
    }
    else {
      throw NonExistentWeaponError(weaponName)
    }
  },

  CreateRandomWeapon() {
    const RandomWeapon = weapons[Math.floor(Math.random() * weapons.length)]
    return new RandomWeapon()
  }

}

module.exports = WeaponFactory

