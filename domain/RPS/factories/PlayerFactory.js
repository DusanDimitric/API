'use strict'

const Player        = require('../Player')
const WeaponFactory = require('./WeaponFactory')

const defaultPlayerName = 'Player'

const PlayerFactory = {

  CreatePlayer({ name = defaultPlayerName, weapon }) {
    return new Player(
      name,
      WeaponFactory.CreateWeapon(weapon)
    )
  },

  CreateComputerPlayers(amount) {
    const computerPlayers = []
    let name, weapon
    for (let i = 0; i < amount; ++i) {
      name   = 'C' + (i + 1)
      weapon = WeaponFactory.CreateRandomWeapon()
      computerPlayers.push(new Player(name, weapon))
    }
    return computerPlayers
  }

}

module.exports = PlayerFactory

